package ponto

import (
	"errors"
	"math/rand"
	"time"
)

type Ponto struct {
	inicio time.Time
	saidaAlmoco time.Time
	voltaAlmoco time.Time
	fim time.Time
	fimDeSemana bool
}

func (p *Ponto) FimDeSemana() bool {
	return p.fimDeSemana
}

func (p *Ponto) Fim() time.Time {
	return p.fim
}

func (p *Ponto) SaidaAlmoco() time.Time {
	return p.saidaAlmoco
}

func (p *Ponto) Inicio() time.Time {
	return p.inicio
}

func (p *Ponto) VoltaAlmoco() time.Time {
	return p.voltaAlmoco
}

func NewRandom(dia, mes, ano int) Ponto {
	inicio := createRandomTime(ano, mes, dia, 9, 10)
	sAlmoco := createRandomTime(ano, mes, dia, 12, 10)
	vAlmoco := createRandomTime(ano, mes, dia, 13, 10)
	fim := createRandomTime(ano, mes, dia, 18, 10)

	return Ponto{
		inicio:      inicio,
		saidaAlmoco: sAlmoco,
		voltaAlmoco: vAlmoco,
		fim:         fim,
		fimDeSemana: isWeekend(inicio),
	}
}

func NewRange(inicio, fim time.Time) ([]Ponto, error) {
	total := fim.YearDay() - inicio.YearDay()
	if total < 0 {
		return nil, errors.New("negativo")
	}

	ret := make([]Ponto, total+1)

	for i := 0; i <= total; i++ {
		a := inicio.Add(time.Duration(i) * time.Hour * 24)
		ret[i] = NewRandom(a.Day(), int(a.Month()), a.Year())
	}

	return ret, nil
}

func isWeekend(inicio time.Time) bool {
	return inicio.Weekday() == time.Saturday || inicio.Weekday() == time.Sunday
}

var r = rand.New(rand.NewSource(time.Now().Unix()))

func createRandomTime(ano, mes, dia, hora, dif int) time.Time {
	base := time.Date(ano, time.Month(mes), dia, hora, 0, 0, 0, time.Local)

	v := r.Intn(dif + 1) - (dif / 2)

	return base.Add(time.Duration(v) * time.Minute)
}
