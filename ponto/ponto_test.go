package ponto

import (
	"math/rand"
	"testing"
	"time"
)

func TestCreateRandomTime(t *testing.T) {
	r = rand.New(rand.NewSource(0))

	want := []time.Time {
		time.Date(2020, 1, 1, 9, 58, 0, 0, time.Local),
		time.Date(2020, 1, 1, 9, 58, 0, 0, time.Local),
		time.Date(2020, 1, 1, 10, 01, 0, 0, time.Local),
	}


	for _, v := range want {
		got := createRandomTime(2020, 1, 1, 10, 10)

		if got != v {
			t.Errorf("wrong time. got %v want %v", got, v)
		}
	}

}

func TestNewRange(t *testing.T) {
	inicio := time.Date(2020,time.January, 1, 0, 0, 0, 0, time.Local)
	fim := time.Date(2020,time.January, 10, 0, 0, 0, 0, time.Local)


	r, _ := NewRange(inicio, fim)

	if len(r) != 10 {
		t.Errorf("wrong amout of days: %d. should be %d", len(r), 10)
	}

	inicioFirstDay := r[0].inicio
	if !sameDate(inicio, inicioFirstDay) {
		t.Errorf("wrong initial day: %v. should be %v", inicio, inicioFirstDay)
	}

	inicioLastDay := r[len(r)-1].inicio
	if !sameDate(fim, inicioLastDay) {
		t.Errorf("wrong final day: %v. should be %v", fim, inicioLastDay)
	}
}

func sameDate(d1, d2 time.Time) bool {
	return d1.YearDay() == d2.YearDay() && d1.Year() == d2.Year()
}
