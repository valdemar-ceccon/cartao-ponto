package main

import (
	"encoding/json"
	"fmt"
	"folhaponto/ponto"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	http.HandleFunc("/", handlePage)
	http.HandleFunc("/api/random", handleApi)
	log.Fatal(http.ListenAndServe(":"+port, nil))

}

func handleApi(writer http.ResponseWriter, request *http.Request) {
	n := time.Now()
	var ini, fim time.Time
	if n.Day() > 25 {
		ini = time.Date(n.Year(), n.Month(), 25, 0, 0, 0, 0, time.Local)
		aux := ini.AddDate(0,1,0)
		fim = time.Date(aux.Year(), aux.Month(), 24, 0, 0, 0, 0, time.Local)
	}
	ran, err := ponto.NewRange(ini, fim)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		_, err := fmt.Fprintf(writer, "{\"message\":\"wrong data range: %s\"}", err.Error())
		must(err)
		return
	}

	must(json.NewEncoder(writer).Encode(createReponse(ran)))
}

type Response struct {
	Ini string `json:"ini"`
	SaidaAlmoco string `json:"saida_almoco"`
	VoltaAlmoco string `json:"volta_almoco"`
	Fim string `json:"fim"`
	FDS bool `json:"fds"`
}

func createReponse(ran []ponto.Ponto) []Response {
	ret := make([]Response, len(ran))
	for i,v := range ran {
		ret[i] = Response{
			Ini:         v.Inicio().Format(time.RFC3339),
			SaidaAlmoco: v.SaidaAlmoco().Format(time.RFC3339),
			VoltaAlmoco: v.VoltaAlmoco().Format(time.RFC3339),
			Fim:         v.Fim().Format(time.RFC3339),
			FDS:         v.FimDeSemana(),
		}
	}
	return ret
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func http500(err error, w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	_, writeErr := w.Write([]byte(err.Error()))
	must(writeErr)
}

type FolhaPonto struct {
	Inicio      string
	Fim         string
	Datas       []LinhaPonto
	EsconderFDS bool
}

type LinhaPonto struct {
	Data      string
	HoraIni   string
	AlmocoIni string
	AlmocoFim string
	HoraFim   string
}

func handlePage(w http.ResponseWriter, r *http.Request) {

	parse, err := template.ParseFiles("index.html")

	if err != nil {
		http500(err, w)
		return
	}
	ini, fim := defaultDates()
	folhaPontoData := FolhaPonto{
		Inicio: ini,
		Fim:    fim,
	}
	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			http500(err, w)
			return
		}
		if data := r.Form.Get("inicio"); data != "" {
			folhaPontoData.Inicio = data
		}

		if data := r.Form.Get("fim"); data != "" {
			folhaPontoData.Fim = data
		}

		if fds := r.Form.Get("esconderfds"); fds != "" {
			folhaPontoData.EsconderFDS = true

		}
	}

	folhaPontoData.gerarData()
	err = parse.Execute(w, folhaPontoData)
	if err != nil {
		http500(err, w)
		return
	}
}

func (p *FolhaPonto) gerarData() {

	var parsedFim, parsedIni time.Time
	var err error
	format := "2006-01-02"

	parsedIni, err = time.Parse(format, p.Inicio)
	must(err)
	parsedFim, err = time.Parse(format, p.Fim)
	must(err)
	for parsedIni.Before(parsedFim) || parsedIni == parsedFim {
		if isWeekend(parsedIni) {
			p.Datas = append(p.Datas, LinhaPonto{Data: parsedIni.Format("02/01/2006")})
		} else {
			p.Datas = append(p.Datas, newRandomLine(parsedIni))
		}
		parsedIni = parsedIni.AddDate(0, 0, 1)
	}

}

func newRandomLine(parsedIni time.Time) LinhaPonto {
	return LinhaPonto{
		Data:      parsedIni.Format("02/01/2006"),
		HoraIni:   randomTime(8, 55),
		AlmocoIni: randomTime(11, 55),
		AlmocoFim: randomTime(12, 55),
		HoraFim:   randomTime(17, 55),
	}
}

func isWeekend(parsedIni time.Time) bool {
	return parsedIni.Weekday() == time.Saturday || parsedIni.Weekday() == time.Sunday
}

var source rand.Source = rand.NewSource(time.Now().Unix())

func randomTime(hora, min int) string {
	r := rand.New(source)
	return time.Date(0, 0, 0, hora, min, 0, 0, time.Local).
		Add(time.Duration(r.Intn(11)) * time.Minute).
		Format("15:04")
}

func defaultDates() (string, string) {
	formatString := "2006-01-02"
	ini := time.Now()
	end := ini.AddDate(0, 1, 0)

	return ini.Format(formatString), end.Format(formatString)
}
